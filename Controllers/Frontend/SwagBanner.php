<?php

class Shopware_Controllers_Frontend_SwagBanner extends Enlight_Controller_Action
{
    public function preDispatch()
    {
        $this->view->addTemplateDir(__DIR__ . '/../../Resources/views');
    }

    public function indexAction()
    {
        $currentAction = $this->Request()->getActionName();
        $this->view->assign('currentAction', $currentAction);
    }

    public function showBanner()
    {
        $currentAction = $this->Request()->getActionName();
        $this->view->assign('currentAction', $currentAction);
    }

}
