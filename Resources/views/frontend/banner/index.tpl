{*content-main--inner*}
{extends file="parent:frontend/index/index.tpl"}
<style>
.banner_covid{
  {if $SwagBanner.imagebackground != ''}
    background-image: url ('{$SwagBanner.imagebackground}');
  {else}
  background: {$SwagBanner.color};
  {/if}
}

</style>
<div class="row">
  <div class="col-sm-12">
    <div class="modal banner_covid" tabindex="-1" role="dialog">
      <div class="modal-dialog">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title">{$SwagBanner.headline}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <p>{$SwagBanner.maintext}</p>
          </div>
          <div class="modal-footer">

            <a href="{$SwagBanner.linkinfo}" >{$SwagBanner.moreinfo}</a>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
