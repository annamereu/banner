<?php

namespace SwagBanner;

use Shopware\Components\Plugin;

class SwagBanner extends Plugin
{
  public function install(InstallContext $context)
  {
      $attributeService = $this->container->get('shopware_attribute.crud_service');

      $attributeService->update(
          's_articles_attributes',
          'alt_image',
          'single_selection',
          [
              'displayInBackend' => true,
              'label' => 'Alternatives Bild',
              'entity' => Media::class
          ]
      );
  }

  public function uninstall(UninstallContext $context)
  {
      if($context->keepUserData()) {
          return;
      }

      $attributeService = $this->container->get('shopware_attribute.crud_service');

      $attributeExists = $attributeService->get(
          's_articles_attributes', 'alt_image'
      );

      if($attributeExists) {
          $attributeService->delete(
              's_articles_attributes',
              'alt_image'
          );
      }
      public function onFrontend(\Enlight_Event_EventArgs $args)
      {
          $this->container->get('Template')->addTemplateDir(
              $this->getPath() . '/Resources/views/'
          );

          /** @var \Enlight_Controller_Action $controller */
          $controller = $args->get('subject');
          $view = $controller->View();
          $view->assign('DSVGO', [
              'headline' => $this->container->get('config')->getByNamespace('SwagBanner','headline'),
              'maintext' => $this->container->get('config')->getByNamespace('SwagBanner','maintext'),
              'color' => $this->container->get('config')->getByNamespace('SwagBanner','color'),
              'imagebackground' => $this->container->get('config')->getByNamespace('SwagBanner','imagebackground'),
              'moreinfo' => $this->container->get('config')->getByNamespace('SwagBanner','moreinfo'),
              'linkinfo' => $this->container->get('config')->getByNamespace('SwagBanner','linkinfo'),
          ]
          );
      }
      $context->scheduleClearCache(InstallContext::CACHE_LIST_ALL);
  }
}
